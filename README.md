# NardnetCommons-mail



## Getting started

## Project status
This project is used to send FAKE, SANDBOX, SMTP emails..

Very important to use of the dependency put in POM like this:

```
      <dependency>
			<groupId>com.nardnet</groupId>
			<artifactId>NardnetCommons-mail</artifactId>
			<version>1.1.3-SNAPSHOT</version>
		</dependency>
		
	 <repositories>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</repository>
	 </repositories>

	<distributionManagement>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</repository>

		<snapshotRepository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</snapshotRepository>
	</distributionManagement>
		
		
```

## Add in your project

```
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;

import com.nardnet.commons.mail.core.domain.ProcessTemplateService;
import com.nardnet.commons.mail.core.service.SendEmailService;
import com.nardnet.commons.mail.infrastructure.EmailCommonsProperties;
import com.nardnet.commons.mail.infrastructure.service.FakeSendEmailService;
import com.nardnet.commons.mail.infrastructure.service.SandboxSendEmailService;
import com.nardnet.commons.mail.infrastructure.service.SmtpSendEmailService;
import com.nardnet.commons.mail.infrastructure.template.FreeMarkerTemplateService;

import freemarker.template.TemplateExceptionHandler;

@Configuration
@ComponentScan(basePackages = "com.nardnet.commons.mail", useDefaultFilters = false)
public class ExternalConfig {

	@Bean
	@Primary
	EmailCommonsProperties emailCommonsProperties() {
		return new EmailCommonsProperties();
	}

	@Bean
	SendEmailService sendEmailService() {

		switch (emailCommonsProperties().getImpl()) {
		case FAKE:
			return new FakeSendEmailService();
		case SMTP:
			return new SmtpSendEmailService();
		case SANDBOX:
			return new SandboxSendEmailService();
		default:
			return null;
		}
	}

	@Bean
	ProcessTemplateService getInstanceTemplate() {

		switch (emailCommonsProperties().getTemplate()) {

		case FREEMARKER:
			return new FreeMarkerTemplateService(emailCommonsProperties(), freeMarkerConfig());
		}

		return null;
	}
	
	@Bean
	@Primary
	freemarker.template.Configuration freeMarkerConfig() {
		freemarker.template.Configuration freeMarkerConfig = null;

		try {

			FreeMarkerConfigurationFactory factory = new FreeMarkerConfigurationFactory();
			factory.setTemplateLoaderPath(emailCommonsProperties().getDir());
			factory.setDefaultEncoding(emailCommonsProperties().getEnconding());
			factory.setPreferFileSystemAccess(false);
			freeMarkerConfig = factory.createConfiguration();
			freeMarkerConfig.setDefaultEncoding(emailCommonsProperties().getEnconding());
			freeMarkerConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			freeMarkerConfig = factory.createConfiguration();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return freeMarkerConfig;
	}

}
```

## Application.properties

- nardnetcommons.mail.sender= [SENDER_EMAIL] 
- nardnetcommons.mail.impl= [FAKE, SMTP, SANDBOX ] 
- nardnetcommons.mail.sandbox-recipient=[ RECIPIENT_EMAIL ]

