package com.nardnet.commons.mail.infrastructure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import com.nardnet.commons.mail.core.domain.Message;
import com.nardnet.commons.mail.core.exception.EmailException;
import com.nardnet.commons.mail.infrastructure.template.EnumTemplateService;

import jakarta.mail.internet.MimeMessage;


public class SmtpSendEmailService extends SendEmailServiceImp {
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Override
	public void send(Message message) {		
		send(message, null);
	}
	
	public void send(Message message, EnumTemplateService templateType) {
		try {
						
			MimeMessage mimeMessage = mailSender.createMimeMessage();				
			toGetHelper(message, mimeMessage);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			throw new EmailException("It was not possible to send the email", e);
		}
	}


	
}
