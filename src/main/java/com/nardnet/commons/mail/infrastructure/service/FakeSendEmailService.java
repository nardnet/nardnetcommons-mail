package com.nardnet.commons.mail.infrastructure.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.nardnet.commons.mail.core.domain.Message;
import com.nardnet.commons.mail.core.domain.ProcessTemplateService;
import com.nardnet.commons.mail.core.service.SendEmailService;

public class FakeSendEmailService implements SendEmailService  {
	
	@Autowired
	ProcessTemplateService strategyTemplateService;
	
	Logger logger = LoggerFactory.getLogger(FakeSendEmailService.class);  	
	
	 @Override
	public void send(Message message) {			 

		 String body = strategyTemplateService.toProcessTemplate(message);

		 logger.info("[FAKE E-MAIL] To: {}\n{}", message.getRecipients(), body);
	}

}
