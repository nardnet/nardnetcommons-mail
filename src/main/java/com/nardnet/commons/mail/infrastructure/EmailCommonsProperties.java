package com.nardnet.commons.mail.infrastructure;

import java.nio.charset.StandardCharsets;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.nardnet.commons.mail.infrastructure.template.EnumTemplateService;

/**
 * {@link ConfigurationProperties @ConfigurationProperties} for configuring NardnetCommons-mail.
 *
 * @author Rafael Gomes 
 * @since 1.1.0
 */
@Validated
@Component
@ConfigurationProperties(prefix = "nardnetcommons.mail")
public class EmailCommonsProperties {
	
	public static final String DEFAULT_TEMPLATE_LOADER_PATH = "classpath:/templates/";
	
	/**
	 * Sender that is allowed to send email.	 
	 */
	private String sender;  
	/**
	 * Encoding defined
	 * Default is UTF-8
	 */	
	private String enconding = StandardCharsets.UTF_8.displayName();
	/**
	 * Directory defined for templates
	 * Default is /templates
	 */
	private String dir = DEFAULT_TEMPLATE_LOADER_PATH;
	/**
	 * Whether to prefer option SANDBOX this property will be necessary
	 * Default Recipient to SANDBOX option. 
	 */
	private String sandboxRecipient;
	/**
	 * Enum with email send options: SMTP, FAKE, SANDBOX 
	 * Default is FAKE
	 */
	private Implementation impl = Implementation.FAKE;
	
	/**
	 * Enum with email send options: FREEMARKER
	 * Default is FREEMARKER
	 */
	private EnumTemplateService template = EnumTemplateService.FREEMARKER;
	
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getEnconding() {
		return enconding;
	}
	public void setEnconding(String enconding) {
		this.enconding = enconding;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
	
	public Implementation getImpl() {
		return impl;
	}
	public void setImpl(Implementation impl) {
		this.impl = impl;
	}	

	public String getSandboxRecipient() {
		return sandboxRecipient;
	}
	public void setSandboxRecipient(String sandboxRecipient) {
		this.sandboxRecipient = sandboxRecipient;
	}	

	public EnumTemplateService getTemplate() {
		return template;
	}
	public void setTemplate(EnumTemplateService template) {
		this.template = template;
	}

	public enum Implementation {
		SMTP, FAKE, SANDBOX
	}
	

}


