package com.nardnet.commons.mail.infrastructure.builder;

import com.nardnet.commons.mail.core.service.SendEmailService;

public final class ClientMessageBuilder extends MessageBuilder<ClientMessageBuilder, SendEmailService> {
	
	public ClientMessageBuilder(SendEmailService sendEmailService) {
		super(sendEmailService);	
	}

	@Override
	public SendEmailService build() {
		sendEmailService.send(message);
		return sendEmailService;
	}

	/**
	 * @return Create new instance of builder with all defaults set.
	 */
	public static MessageBuilder<ClientMessageBuilder, SendEmailService> standard(SendEmailService sendEmailService) {
		return new ClientMessageBuilder(sendEmailService);
	}

}
