package com.nardnet.commons.mail.infrastructure.builder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.nardnet.commons.mail.core.domain.Message;
import com.nardnet.commons.mail.core.service.SendEmailService;

/**
 * Base class for all service specific client builders.
 *
 * @param <Subclass>    Concrete builder type, used for better fluent methods.
 * @param <TypeToBuild> Type that this builder builds.
 */
public abstract class MessageBuilder<Subclass extends MessageBuilder<Subclass, TypeToBuild>, TypeToBuild extends SendEmailService> {
	
	SendEmailService sendEmailService;
	Message message;
	Map<String, Object> variables;	
		
	public MessageBuilder(SendEmailService sendEmailService) {		
		this.sendEmailService = sendEmailService;

		message = new Message();
		variables = new HashMap<>();
	}

	public Subclass addVariables(Map<String, Object> map) {
		variables.putAll(map);
		message.setVariables(variables);
		return getSubclass();
	}
	
	public Subclass addVariables(String key, Object Value) {
		variables.put(key, Value);
		message.setVariables(variables);
		return getSubclass();
	}
	
	public Subclass withBody(String body) {
		message.setBody(body);
		return getSubclass();
	}
	
	public Subclass withAddress(String... address) {		
		var mySet = new HashSet<>(Arrays.asList(address));
		message.setRecipients(mySet);
		return getSubclass();
	}
	
	public Subclass withSubject(String subject) {
		message.setSubject(subject);
		return getSubclass();
	}

	@SuppressWarnings("unchecked")
	protected final Subclass getSubclass() {
		return (Subclass) this;
	}

	/**
	 * Builds a client with the configure properties.
	 *
	 * @return Client instance to make API calls with.
	 */
	public abstract TypeToBuild build();

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	
	
}
