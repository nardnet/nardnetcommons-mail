package com.nardnet.commons.mail.infrastructure.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import com.nardnet.commons.mail.core.domain.Message;
import com.nardnet.commons.mail.core.exception.EmailException;

import jakarta.mail.internet.MimeMessage;

public class SandboxSendEmailService extends SendEmailServiceImp {
		
	@Autowired
	private JavaMailSender mailSender;	
	
	@Override
	public void send(Message message) {		
		try {
			
			if (Objects.isNull(getEmailProperties().getSandboxRecipient())) {				
				   throw new NullPointerException("nardnetcommons.mail.sandbox-recipient is null");				
			}
			
			Set<String> recipients = new HashSet<>(Arrays.asList(getEmailProperties().getSandboxRecipient().split(";")));		
			
			message.setRecipients(recipients);	
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();	
			
			toGetHelper(message,  mimeMessage);

			mailSender.send(mimeMessage);
		} catch (Exception e) {
			throw new EmailException("It was not possible to send the email", e);
		}
	}

}
