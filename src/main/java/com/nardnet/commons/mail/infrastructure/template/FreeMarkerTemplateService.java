package com.nardnet.commons.mail.infrastructure.template;

import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.nardnet.commons.mail.core.domain.Message;
import com.nardnet.commons.mail.core.domain.ProcessTemplateService;
import com.nardnet.commons.mail.core.exception.EmailException;
import com.nardnet.commons.mail.infrastructure.EmailCommonsProperties;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreeMarkerTemplateService implements ProcessTemplateService {

	EmailCommonsProperties emailProperties;

	Configuration freeMarkerConfig;

	public FreeMarkerTemplateService(EmailCommonsProperties emailProperties, Configuration freeMarkerConfig) {
		this.emailProperties = emailProperties;
		this.freeMarkerConfig = freeMarkerConfig;
	}

	@Override
	public String toProcessTemplate(Message message) {
		try {
			Template template = freeMarkerConfig.getTemplate(message.getBody());
			return FreeMarkerTemplateUtils.processTemplateIntoString(template, message.getVariables());

		} catch (Exception e) {
			throw new EmailException("Unable to create email template", e);
		}
	}

}
