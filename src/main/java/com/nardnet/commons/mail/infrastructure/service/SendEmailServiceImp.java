package com.nardnet.commons.mail.infrastructure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.nardnet.commons.mail.core.domain.Message;
import com.nardnet.commons.mail.core.domain.ProcessTemplateService;
import com.nardnet.commons.mail.core.service.SendEmailService;
import com.nardnet.commons.mail.infrastructure.EmailCommonsProperties;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;


public class SendEmailServiceImp implements SendEmailService {	

	@Autowired
	private EmailCommonsProperties emailProperties;		

	@Autowired
	ProcessTemplateService strategyTemplateService;		

	public void toGetHelper(Message message, MimeMessage mimeMessage)
			throws MessagingException {
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, emailProperties.getEnconding());		

		helper.setFrom(emailProperties.getSender());
		helper.setSubject(message.getSubject());
		helper.setText(strategyTemplateService.toProcessTemplate(message), true);
		helper.setTo(message.getRecipients().toArray(new String[0]));
	}

	public EmailCommonsProperties getEmailProperties() {
		return emailProperties;
	}

	public void setEmailProperties(EmailCommonsProperties emailProperties) {
		this.emailProperties = emailProperties;
	}

	@Override
	public void send(Message message) {}	

}
