package com.nardnet.commons.mail.infrastructure.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nardnet.commons.mail.core.service.SendEmailService;
import com.nardnet.commons.mail.infrastructure.builder.ClientMessageBuilder;

@RestController
@RequestMapping("/api")
public class SendResourceTest {

	@Autowired
	SendEmailService emailService;

	@GetMapping("/data")
	public ResponseEntity<HttpStatus> getData() {

		Map<String, Object> variable = new HashMap<>();
		variable.put("nameClient", "Rafael Carvalho");

		ClientMessageBuilder.standard(emailService).withAddress("test@gmail.com").withSubject("Restaurate teste")
				.withBody("test-template.html").addVariables(variable).build();

		return ResponseEntity.ok(HttpStatus.OK);
	}

}
