package com.nardnet.commons.mail.infrastructure.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;

import com.nardnet.commons.mail.core.domain.ProcessTemplateService;
import com.nardnet.commons.mail.core.service.SendEmailService;
import com.nardnet.commons.mail.infrastructure.EmailCommonsProperties;
import com.nardnet.commons.mail.infrastructure.service.FakeSendEmailService;
import com.nardnet.commons.mail.infrastructure.service.SandboxSendEmailService;
import com.nardnet.commons.mail.infrastructure.service.SmtpSendEmailService;
import com.nardnet.commons.mail.infrastructure.template.FreeMarkerTemplateService;

import freemarker.template.TemplateExceptionHandler;

@Configuration
public class EmailCommonsConfig {

	Logger logger = LoggerFactory.getLogger(EmailCommonsConfig.class);

	@Autowired
	private EmailCommonsProperties emailProperties;

	@Bean
	SendEmailService sendEmailService() {

		logger.info("[EmailCommonsProperties bean sendEmailService] must be: {}", emailProperties.getImpl());

		return switch (emailProperties.getImpl()) {
		case FAKE -> new FakeSendEmailService();
		case SMTP -> new SmtpSendEmailService();
		case SANDBOX -> new SandboxSendEmailService();	
		};
	}

	@Bean
	ProcessTemplateService getInstanceTemplate() {

		logger.info("[EmailCommonsProperties bean getInstanceTemplate] must be: {}", emailProperties.getTemplate());

		return switch (emailProperties.getTemplate()) {
		case FREEMARKER -> new FreeMarkerTemplateService(emailProperties, freeMarkerConfig());
		};
	}

	@Bean
	freemarker.template.Configuration freeMarkerConfig() {
		freemarker.template.Configuration freeMarkerConfig = null;

		try {

			FreeMarkerConfigurationFactory factory = new FreeMarkerConfigurationFactory();
			factory.setTemplateLoaderPath(emailProperties.getDir());
			factory.setDefaultEncoding(emailProperties.getEnconding());
			factory.setPreferFileSystemAccess(false);
			freeMarkerConfig = factory.createConfiguration();
			freeMarkerConfig.setDefaultEncoding(emailProperties.getEnconding());
			freeMarkerConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			freeMarkerConfig = factory.createConfiguration();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return freeMarkerConfig;
	}

}
