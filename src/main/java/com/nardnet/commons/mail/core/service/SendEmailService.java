package com.nardnet.commons.mail.core.service;

import com.nardnet.commons.mail.core.domain.Message;

public interface SendEmailService {

	void send(Message message);
	
}
