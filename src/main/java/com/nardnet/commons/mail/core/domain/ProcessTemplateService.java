package com.nardnet.commons.mail.core.domain;

public interface ProcessTemplateService {
	
	String toProcessTemplate(Message message);

}
